﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MyPriced.Domain;

namespace MyPriced.Main.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/{v:apiVersion}/products")]
    public class ProductsController : ControllerBase
    {

        private readonly MyPricedDbContext _context;

        public ProductsController(MyPricedDbContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return _context.Products.ToList();
        }
    }
}

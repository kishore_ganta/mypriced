﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MyPriced.Domain;

namespace MyPriced.Main.Controllers
{
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/{v:apiVersion}/products")]
    public class Products2Controller : ControllerBase
    {

        private readonly MyPricedDbContext _context;

        public Products2Controller(MyPricedDbContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return _context.Products.Take(10).ToList();
        }
    }
}
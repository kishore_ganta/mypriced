﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPriced.Domain
{
    public class ProductDescription
    {
        public int ProductDescriptionId { get; set; }
        public string DescriptionLine1 { get; set; }
        public string DescriptionLine2 { get; set; }

        public int DescriptionOfProductId { get; set; }
        public Product Product { get; set; }
    }
}

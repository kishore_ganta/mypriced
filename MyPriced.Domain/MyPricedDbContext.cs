﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyPriced.Domain
{
    public class MyPricedDbContext:DbContext
    {
        public MyPricedDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasOne(p => p.Description)
                .WithOne(d => d.Product)
                .HasForeignKey<ProductDescription>(d => d.DescriptionOfProductId);
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductDescription> ProductDescriptions { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }       

    }
}

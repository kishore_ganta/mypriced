﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyPriced.Domain
{
    public class ProductCategory
    {
        public int ProductCategoryId { get; set; }
        public string CategoryName { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
